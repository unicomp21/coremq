package coremq;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.spi.AbstractSelector;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.UUID;

public class UdpSessions implements ISocketNotify, ISessionsMessaging, IMessageEvent {
	private UdpSessions() { }
	private UdpSessions(AbstractSelector selector, InetSocketAddress inetSocketAddress) throws IOException {
		_selector = selector;
		_inetSocketAddress = inetSocketAddress;
		_datagramChannel = DatagramChannel.open();
		_datagramChannel.bind(_inetSocketAddress);
		_datagramChannel.configureBlocking(false);
		_datagramChannel.register(_selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE, this);
		_receiver = new UdpReceiver(_datagramChannel, this);
		_sender = new UdpSender(_datagramChannel);
		_incomingSessionMessages = new LinkedList<IMessage>();		
	}
	private AbstractSelector _selector;
	private DatagramChannel _datagramChannel;
	private InetSocketAddress _inetSocketAddress;
	private ISender _sender;
	private IReceiver _receiver;
	private LinkedList<IMessage> _incomingSessionMessages;
	public static ISessionsMessaging open(AbstractSelector selector, InetSocketAddress inetSocketAddress) throws IOException {
		return new UdpSessions(selector, inetSocketAddress);
	}
	@Override
	public void Received(IMessage iMessage, SocketAddress inetSocketAddress) {
		//((ITextRender)iMessage).Dump(System.out, 0);
		String sessionId = iMessage.GetValue(Tags.SessionId);
		if(null != sessionId) {
			if(!_sessions.containsKey(sessionId)) {
				_sessions.put(sessionId, (InetSocketAddress)inetSocketAddress);
				IMessage iMessageAccept = new Message();
				iMessageAccept.PutValue(Tags.Command, Tags.Commands.Accepted);
				iMessageAccept.PutValue(Tags.SessionId, sessionId);
				_incomingSessionMessages.add(iMessageAccept);
			}
			_incomingSessionMessages.add(iMessage);
		}
	}
	private final Hashtable<String /*SessionId*/, InetSocketAddress> _sessions = new Hashtable<String, InetSocketAddress>(); 
	@Override
	public void Connect(InetSocketAddress inetSocketAddress) throws Exception {
		UUID uuid = UUID.randomUUID();
		String sessionId = uuid.toString();
		_sessions.put(sessionId, inetSocketAddress);

		IMessage iMessage = new Message();
		iMessage.PutValue(Tags.Command, Tags.Commands.Connected);
		iMessage.PutValue(Tags.SessionId, sessionId);
		_incomingSessionMessages.add(iMessage);
	}
	@Override
	public IMessage GetMessage() {
		return _incomingSessionMessages.isEmpty() ?
				null : _incomingSessionMessages.removeFirst();
	}
	@Override
	public boolean SendMessage(IMessage iMessage) {
		try {
			String sessionId = iMessage.GetValue(Tags.SessionId);
			if(null != sessionId) {
				InetSocketAddress inetSocketAddress = _sessions.get(sessionId);
				if(null != inetSocketAddress) {
					iMessage.PutValue(Tags.SendTo, inetSocketAddress.toString());
					_sender.SendMessage(iMessage);
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			//e.printStackTrace();
			return false;
		}
	}
	@Override
	public void AcceptSignaled() {
		// not used, UDP
	}
	@Override
	public void ConnectSignaled() {
		// not used, UDP
	}
	@Override
	public void ReadSignaled() {
		_receiver.Flush();
	}
	public void WriteSignaled() {
		try {
			_sender.Flush();
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
}
