package coremq;

import java.io.PrintStream;
import java.nio.CharBuffer;
import java.util.Hashtable;

public class Message implements IMessage, IReadWrite, ITextRender {
	private Hashtable<String, StringBuilder> _tagValues = new Hashtable<String, StringBuilder>();
	@Override
	public String GetValue(String key) {
		if(!_tagValues.containsKey(key))
			return null;
		else
			return _tagValues.get(key).toString();
	}
	public final int initialValueCapacity = 16;
	@Override
	public void PutValue(String key, String value) {
		StringBuilder sb = _tagValues.get(key);
		if(null == sb) {
			sb = new StringBuilder(initialValueCapacity);
			_tagValues.put(key, sb);
		}
		sb.setLength(0);
		sb.append(value);
		_tagValues.put(key, sb);
	}
	private Hashtable<String, IMessage> _tagMessages = new Hashtable<String, IMessage>();
	@Override
	public IMessage GetMessage(String key) {
		return _tagMessages.get(key);
	}
	@Override
	public IMessage AllocMessage(String key) {
		if(_tagMessages.containsKey(key)) {
			return _tagMessages.get(key);
		} else {
			Message message = new Message();
			_tagMessages.put(key, message);
			return message;
		}
	}
	@Override
	public void PutMessage(String key, IMessage message) {
		_tagMessages.put(key, message);
	}
	@Override
	public boolean Read(CharBuffer reader) {
		IPrefixedStringReader stringReader = new PrefixedStringReader(reader);
		return Read(stringReader);
	}
	private boolean ReadTagValues(IPrefixedStringReader stringReader) {
		String tagCountText = stringReader.ReadShortString();
		if(null != tagCountText) {
			int tagCountInteger = Integer.parseInt(tagCountText);
			int i = 0;
			for(; i < tagCountInteger; i++) {
				String tag = stringReader.ReadLargeString();
				if(null != tag) {
					String val = stringReader.ReadLargeString();
					if(null != val) {
						StringBuilder sb = new StringBuilder(initialValueCapacity);
						sb.append(val);
						_tagValues.put(tag, sb);
					}
				}
			}
			if(i == tagCountInteger)
				return true;
		}
		return false;
	}
	private boolean ReadTagMessages(IPrefixedStringReader stringReader) {
		String tagCountText = stringReader.ReadShortString();
		if(null != tagCountText) {
			int tagCountInteger = Integer.parseInt(tagCountText);
			int i = 0;
			for(; i < tagCountInteger; i++) {
				String tag = stringReader.ReadLargeString();
				if(null != tag) {
					IReadWrite iReadWrite = new Message();
					if(iReadWrite.Read(stringReader)) {
						_tagMessages.put(tag, (IMessage)iReadWrite);
					}
				}
			}
			if(i == tagCountInteger)
				return true;
		}
		return false;		
	}
	@Override
	public void Write(CharBuffer writer) throws Exception {
		IPrefixedStringWriter stringWriter = new PrefixedStringWriter(writer);
		Write(stringWriter);
	}
	@Override
	public void Dump(PrintStream printStream, int indent) {
		if(0 == indent)
			printStream.println("---");
		StringBuilder stringBuilder = new StringBuilder();
		for(String key : _tagValues.keySet()) {
			stringBuilder.setLength(0);
			for(int i = 0; i < indent; i++)
				stringBuilder.append("\t");
			stringBuilder.append(key);
			stringBuilder.append(": ");
			stringBuilder.append(_tagValues.get(key));
			printStream.println(stringBuilder.toString());
		}
		for(String key : _tagMessages.keySet()) {
			stringBuilder.setLength(0);
			for(int i = 0; i < indent; i++)
				stringBuilder.append("\t");
			stringBuilder.append(key);
			stringBuilder.append(": ");
			stringBuilder.append("\r\n");
			ITextRender iTextRender = (ITextRender)_tagMessages.get(key);
			iTextRender.Dump(printStream, indent + 1);
		}
	}
	@Override
	public boolean Read(IPrefixedStringReader stringReader) {
		if(ReadTagValues(stringReader))
			if(ReadTagMessages(stringReader))
				return true;
		return false;
	}
	@Override
	public void Write(IPrefixedStringWriter stringWriter) throws Exception {
		// tag/values
		stringWriter.WriteShortString(Integer.toString(_tagValues.size()));
		for(String key : _tagValues.keySet()) {
			stringWriter.WriteLargeString(key);
			stringWriter.WriteLargeString(_tagValues.get(key).toString());
		}
		
		// tag/messages
		stringWriter.WriteShortString(Integer.toString(_tagMessages.size()));
		for(String key : _tagMessages.keySet()) {
			stringWriter.WriteLargeString(key);
			IReadWrite iReadWrite = (IReadWrite)_tagMessages.get(key);
			iReadWrite.Write(stringWriter);
		}
	}
	@Override
	public void Merge(IMessage iMessageSrc) {
		//todo, unit/perf tests
		Message messageSrc = (Message)iMessageSrc;
		for(String key : messageSrc._tagValues.keySet()) {
			StringBuilder sbSrc = messageSrc._tagValues.get(key);
			StringBuilder sbDst = _tagValues.get(key);
			sbDst.setLength(0);
			sbDst.append(sbSrc);
		}
		for(String key : messageSrc._tagMessages.keySet()) {
			IMessage iSubMessageSrc = messageSrc._tagMessages.get(key);
			IMessage iSubMessageDst = _tagMessages.get(key);
			iSubMessageDst.Merge(iSubMessageSrc);
		}
	}
}
