package coremq;

public interface ISender {
	void Flush() throws Exception;
	void SendMessage(IMessage iMessage) throws Exception;
}
