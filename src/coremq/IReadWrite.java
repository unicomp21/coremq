package coremq;

import java.nio.CharBuffer;

public interface IReadWrite {
	boolean Read(CharBuffer reader);
	boolean Read(IPrefixedStringReader stringReader);
	void Write(CharBuffer writer) throws Exception;
	void Write(IPrefixedStringWriter stringWriter) throws Exception;
}
