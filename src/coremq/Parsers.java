package coremq;

import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;

public class Parsers {
	public static InetSocketAddress ParseInetSockeAddress(String inetSocketAddress) throws URISyntaxException {
		URI uri = new URI("http:/" + inetSocketAddress);
		return new InetSocketAddress(uri.getHost(), uri.getPort());
	}
}
