package coremq;

import java.util.BitSet;
import coremq.WebsocketFrameOpcodes;

public class WebsocketFrame implements IWebsocketFrameHeader {
	private final int HeaderByteCount = 2;
	private final BitSet _bitSet = new BitSet(HeaderByteCount * Constants.BitsPerByte);
	private final BitFields _bitFields;
	// http://tools.ietf.org/html/rfc6455
	public WebsocketFrame() {
		_bitFields = new BitFields(_bitSet);
	}
	@Override
	public void setFIN(boolean val) {
		_bitSet.set(0, val);
	}
	@Override
	public boolean getFIN() {
		return _bitSet.get(0);
	}
	@Override
	public void setRSV1(boolean val) {
		_bitSet.set(1, val);
	}
	@Override
	public boolean getRSV1() {
		return _bitSet.get(1);
	}
	@Override
	public void setRSV2(boolean val) {
		_bitSet.set(2, val);
	}
	@Override
	public boolean getRSV2() {
		return _bitSet.get(2);
	}
	@Override
	public void setRSV3(boolean val) {
		_bitSet.set(3, val);
	}
	@Override
	public boolean getRSV3() {
		return _bitSet.get(3);
	}
	@Override
	public void setOpcode(int opcode) { // WebsocketFrameOpcodes
		switch(opcode) {
		case WebsocketFrameOpcodes.ContinuationFrame:
		case WebsocketFrameOpcodes.TextFrame:
		case WebsocketFrameOpcodes.BinaryFrame:
		case WebsocketFrameOpcodes.ConnectionClose:
		case WebsocketFrameOpcodes.Ping:
		case WebsocketFrameOpcodes.Pong:
			_bitFields.setField(4, 4, opcode);
			break;
		default:
			throw new IllegalArgumentException();
		}
	}
	@Override
	public int getOpcode() { // WebsocketFrameOpcodes
		return _bitFields.getField(4, 4);
	}
	@Override
	public void setShortPayloadLen(byte val) {
		
	}
	@Override
	public byte getShortPayloadLen() {
		// TODO Auto-generated method stub
		return 0;
	}
}
