package coremq;

public class Tags {
	public final static String Command = "command";
	public static class Commands {
		public final static String Connected = new String("connected");
		public final static String Accepted = new String("accepted");
		public final static String Echo = new String("echo");
		public final static String SessionErase = new String("session_erase");
	}
	public final static String SessionId = new String("session_id");
	public final static String SeqNo = new String("seqno");
	public final static String Tag = new String("tag");
	public final static String SendTo = new String("send_to");
}
