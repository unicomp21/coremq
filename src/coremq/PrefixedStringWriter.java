package coremq;

import java.nio.CharBuffer;

public class PrefixedStringWriter implements IPrefixedStringWriter {
	@SuppressWarnings("unused")
	private PrefixedStringWriter() {
	}
	private CharBuffer _writer;
	public PrefixedStringWriter(CharBuffer writer) {
		_writer = writer;
	}
	@Override
	public void WriteShortString(String val) throws Exception {
		if(val.length() > Constants.MaxString)
			throw new Exception("string too long, > 25");
		final int prefixCharCount = 1;
		if((val.length() + prefixCharCount) > _writer.remaining())
			throw new Exception("buffer exhausted");
		char length = (char) (val.length() + 'a');
		_writer.put(length);
		_writer.put(val.toCharArray());
	}
	@Override
	public void WriteLargeString(String val) throws Exception {
		WriteShortString(Integer.toString(val.length()));
		_writer.put(val.toCharArray());
	}
}
