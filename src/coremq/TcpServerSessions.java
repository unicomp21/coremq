package coremq;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelector;
import java.util.Hashtable;
import java.util.LinkedList;

public class TcpServerSessions implements ISocketNotify, ISessionsMessaging {
	private TcpServerSessions(InetSocketAddress inetSocketAddress, AbstractSelector selector) throws IOException {
		_selector = selector;
		_serverSocketChannel = ServerSocketChannel.open();
		_serverSocketChannel.bind(inetSocketAddress);
		_serverSocketChannel.configureBlocking(false);
		_serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT).attach(this);		
	}
	private AbstractSelector _selector;
	public static ISessionsMessaging open(InetSocketAddress inetSocketAddress, AbstractSelector selector) throws IOException {
		return new TcpServerSessions(inetSocketAddress, selector);
	}
	private ServerSocketChannel _serverSocketChannel;
	private Hashtable<String/*SessionId*/, ISession> _sessions = new Hashtable<String, ISession>();
	private LinkedList<IMessage> _incomingSessionMessages = new LinkedList<IMessage>();
	@Override
	public IMessage GetMessage() {
		if(!_incomingSessionMessages.isEmpty()) {
			IMessage iMessage = _incomingSessionMessages.removeFirst();
			if(!SessionErase(iMessage))
				return iMessage;
		}
		return null;
	}
	private boolean SessionErase(IMessage iMessage) {
		String command = iMessage.GetValue(Tags.Command);
		if((null != command) && command.equals(Tags.Commands.SessionErase)) {
			String sessionId = iMessage.GetValue(Tags.SessionId);
			if(null != sessionId) {
				ISession iSession = _sessions.remove(sessionId);
				iSession.Close();
				System.out.println("SessionErase: " + sessionId);
				return true;
			}
		}
		return false;
	}
	@Override
	public boolean SendMessage(IMessage iMessage) {
		String sessionId = iMessage.GetValue(Tags.SessionId);
		if(null != sessionId) {
			ISession iSession = _sessions.get(sessionId);
			if(null != iSession)
				return iSession.SendMessage(iMessage);
		}
		return false;
	}
	@Override
	public void AcceptSignaled() {
		try {
			SocketChannel socketChannel = _serverSocketChannel.accept();
			if(null != socketChannel) {
				ISession iSession = TcpSession.ServerSession(_selector, socketChannel, _incomingSessionMessages);
				_sessions.put(iSession.SessionId(), iSession);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void ConnectSignaled() {	/*NOOP*/ }
	@Override
	public void ReadSignaled() { /*NOOP*/ }
	@Override
	public void WriteSignaled() { /*NOOP*/ }
	@Override
	public void Connect(InetSocketAddress inetSocketAddress) throws Exception {
		throw new Exception("not implemented");
	}
	//todo, cleanup on disconnect, graceful or abrupt
}
