package coremq;

import java.net.InetSocketAddress;

public interface ISessionsMessaging {
	void Connect(InetSocketAddress inetSocketAddress) throws Exception;
	IMessage GetMessage();
	boolean SendMessage(IMessage iMessage);
}
