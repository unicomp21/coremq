package coremq;

import java.util.BitSet;

public class BitFields implements IBitFields {
	@SuppressWarnings("unused")
	private BitFields() { }
	public BitFields(BitSet bits) {
		_bits = bits;
	}
	private BitSet _bits;
	@Override
	public int getField(int size, int offset) {
		int val = 0;
		for(int i = offset; i < (offset + size); i++) {
			val <<= 1;
			val |= _bits.get(i) ? 1 : 0;
		}
		return val;
	}
	@Override
	public void setField(int size, int offset, int val) {
		for(int i = (offset + size); i >= offset; i--) {
			boolean bit = ((val & 0x1) == 1) ? true : false;
			_bits.set(i, bit);
		}
	}
}
