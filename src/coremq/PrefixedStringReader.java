package coremq;

import java.nio.CharBuffer;

public class PrefixedStringReader implements IPrefixedStringReader {
	@SuppressWarnings("unused")
	private PrefixedStringReader() {
	}
	private CharBuffer _reader;
	public PrefixedStringReader(CharBuffer reader) {
		_reader = reader;
	}
	public int BytePosition() {
		return _reader.position() * Constants.SizeOfChar;
	}
	public String ReadShortString() {
		try {
			// base 26 length, a-z
			if(_reader.remaining() > 0) {
				int length = _reader.get();
				length -= 'a';
				if(length > Constants.MaxString)
					throw new Exception("malformed string prefix");
				if(length <= _reader.remaining()) {
					char [] buffer = new char[length];
					_reader.get(buffer);
					return new String(buffer);
				}
			}
		} catch(Exception e) { }
		return null;
	}
	public String ReadLargeString() {
		String lengthPrefixString = ReadShortString();
		if(null != lengthPrefixString) {
			int lengthPrefixInteger = Integer.parseInt(lengthPrefixString);
			if(lengthPrefixInteger <= _reader.remaining()) {
				char[] buffer = new char[lengthPrefixInteger];
				if(null != buffer) {
					_reader.get(buffer);
					return new String(buffer);
				}
			}
		}
		return null;
	}
}
