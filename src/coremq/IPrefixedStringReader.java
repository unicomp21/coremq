package coremq;

public interface IPrefixedStringReader {
	String ReadShortString();
	String ReadLargeString();
	int BytePosition();
}
