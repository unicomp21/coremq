package coremq;

public interface IBitFields {
	int getField(int size, int offset);
	void setField(int size, int offset, int val);
}
