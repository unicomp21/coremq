package coremq;

import java.util.HashSet;

public interface ILastValue {
	long GetSeqNo();
	void Merge(IMessage iMessageUpdate);
	IMessage DeepCopy();
	HashSet<String> Subscribers();
}
