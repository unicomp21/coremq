package coremq;

public class WebsocketFrameOpcodes {
	public final static byte ContinuationFrame = 0x0;
	public final static byte TextFrame = 0x1;
	public final static byte BinaryFrame = 0x2;
	//public final static byte ReservedNonControlFrames, 0x3-0x7
	public final static byte ConnectionClose = 0x8;
	public final static byte Ping = 0x9;
	public final static byte Pong = 0xa;
	//ReservedControlFrames, 0xb - 0xf
}
