package coremq;

import java.net.SocketAddress;

public interface IMessageEvent {
	void Received(IMessage iMessage, SocketAddress inetSocketAddress);
}
