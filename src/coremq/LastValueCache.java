package coremq;

import java.util.Hashtable;

public class LastValueCache implements ILastValueCache {
	private Hashtable<String /*topic*/, ILastValue> _lastValues = new Hashtable<String, ILastValue>();
	@Override
	public ILastValue Get(String topic) {
		if(_lastValues.containsKey(topic))
			return _lastValues.get(topic);
		else
			return null;
	}
	@Override
	public void Update(String topic, IMessage iMessageUpdate) {
		if(!_lastValues.containsKey(topic))
			_lastValues.put(topic, new LastValue());
		_lastValues.get(topic).Merge(iMessageUpdate);
	}
}
