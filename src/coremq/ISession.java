package coremq;

public interface ISession {
	String SessionId();
	boolean SendMessage(IMessage iMessage);
	void Close();
}
