package coremq;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ByteChannel;
import java.util.LinkedList;

public class TcpSender implements ISender {
	@SuppressWarnings("unused")
	private TcpSender() { }
	private ByteChannel _socketChannel;
	private ByteBuffer _outgoingBuffer = ByteBuffer.allocate(Constants.MaxMessageSize);
	public TcpSender(ByteChannel socketChannel) {
		_socketChannel = socketChannel;
		_outgoingBuffer.position(_outgoingBuffer.limit());
	}
	private LinkedList<IMessage> _outgoingMessages = new LinkedList<IMessage>();
	@Override
	public void Flush() throws Exception {
		if(_outgoingBuffer.hasRemaining()) {
			_socketChannel.write(_outgoingBuffer);
		} else {
			if(!_outgoingMessages.isEmpty()) {
				IReadWrite iMessage = (IReadWrite)_outgoingMessages.removeFirst();
				_outgoingBuffer = ByteBuffer.allocate(Constants.MaxMessageSize);
				CharBuffer charBuffer = _outgoingBuffer.asCharBuffer();
				iMessage.Write(charBuffer);
				_outgoingBuffer.position(charBuffer.position() * Constants.SizeOfChar);
				_outgoingBuffer.flip();
				_socketChannel.write(_outgoingBuffer);
			}
		}
	}
	@Override
	public void SendMessage(IMessage iMessage) throws Exception {
		_outgoingMessages.add(iMessage);
		Flush();
	}
}
