package coremq;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.DatagramChannel;
import java.util.LinkedList;

public class UdpSender implements ISender {
	@SuppressWarnings("unused")
	private UdpSender() { }
	private DatagramChannel _socketChannel;
	private ByteBuffer _outgoingBuffer = ByteBuffer.allocate(Constants.MaxMessageSize);
	public UdpSender(DatagramChannel socketChannel) {
		_socketChannel = socketChannel;
		_outgoingBuffer.position(_outgoingBuffer.limit());
	}
	private LinkedList<IMessage> _outgoingMessages = new LinkedList<IMessage>();
	@Override
	public void Flush() throws Exception {
		if(_outgoingBuffer.hasRemaining()) {
			_socketChannel.write(_outgoingBuffer);
		} else {
			if(!_outgoingMessages.isEmpty()) {
				IReadWrite iMessage = (IReadWrite)_outgoingMessages.removeFirst();
				String sendTo = ((IMessage)iMessage).GetValue(Tags.SendTo);
				if(null != sendTo) {
					_outgoingBuffer = ByteBuffer.allocate(Constants.MaxMessageSize);
					CharBuffer charBuffer = _outgoingBuffer.asCharBuffer();
					iMessage.Write(charBuffer);
					_outgoingBuffer.position(charBuffer.position() * Constants.SizeOfChar);
					_outgoingBuffer.flip();
					//((ITextRender)iMessage).Dump(System.out, 0);
					_socketChannel.send(_outgoingBuffer, Parsers.ParseInetSockeAddress(sendTo));
				}
			}
		}
	}
	@Override
	public void SendMessage(IMessage iMessage) throws Exception {
		_outgoingMessages.add(iMessage);
		Flush();
	}
}
