package coremq;

public interface ISocketNotify {
	void AcceptSignaled();
	void ConnectSignaled();
	void ReadSignaled();
	void WriteSignaled();
}
