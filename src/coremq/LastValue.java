package coremq;

import java.util.HashSet;

public class LastValue implements ILastValue {
	private final IMessage _iMessage = new Message();
	private long _seqno = 0;
	@Override
	public long GetSeqNo() {
		return _seqno;
	}
	@Override
	public void Merge(IMessage iMessageUpdate) {
		_iMessage.Merge(iMessageUpdate);
	}
	@Override
	public IMessage DeepCopy() {
		IMessage iMessageCopy = new Message();
		iMessageCopy.Merge(_iMessage);
		iMessageCopy.PutValue(Tags.SeqNo, Long.toString(_seqno));
		_seqno++;
		return iMessageCopy;
	}
	private final HashSet<String> _subscribers = new HashSet<String>();
	@Override
	public HashSet<String /*SessionId*/> Subscribers() {
		return _subscribers;
	}
}
