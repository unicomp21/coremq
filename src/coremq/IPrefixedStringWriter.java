package coremq;

public interface IPrefixedStringWriter {
	void WriteShortString(String val) throws Exception;
	void WriteLargeString(String val) throws Exception;
}
