package coremq;

public interface IMessage {
	// this node, tag/value
	String GetValue(String key);
	void PutValue(String key, String value);
	// child nodes, tag/message
	IMessage GetMessage(String key);
	void PutMessage(String key, IMessage message);
	IMessage AllocMessage(String key);
	void Merge(IMessage iMessageSrc);
	//todo, blob
}
