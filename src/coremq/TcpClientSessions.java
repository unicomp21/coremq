package coremq;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.spi.AbstractSelector;
import java.util.Hashtable;
import java.util.LinkedList;

public class TcpClientSessions implements ISessionsMessaging, IMessageEvent {
	private TcpClientSessions() { }
	private AbstractSelector _selector;
	public static ISessionsMessaging open(AbstractSelector selector) {
		TcpClientSessions clientSessions = new TcpClientSessions();
		clientSessions._selector = selector;
		return clientSessions;
	}
	private Hashtable<String/*SessionId*/, ISession> _sessions = new Hashtable<String, ISession>();
	private LinkedList<IMessage> _incomingSessionMessages = new LinkedList<IMessage>();
	@Override
	public IMessage GetMessage() {
		return _incomingSessionMessages.isEmpty() ?
				null : _incomingSessionMessages.removeFirst();
	}
	@Override
	public boolean SendMessage(IMessage iMessage) {
		String sessionId = iMessage.GetValue(Tags.SessionId);
		if(null != sessionId) {
			ISession iSession = _sessions.get(sessionId);
			if(null != iSession)
				return iSession.SendMessage(iMessage);
		}
		return false;
	}
	@Override
	public void Received(IMessage iMessage, SocketAddress inetSocketAddress) {
		_incomingSessionMessages.add(iMessage);
	}
	@Override
	public void Connect(InetSocketAddress inetSocketAddress) throws IOException {
		ISession iSession = TcpSession.ClientSession(
				_selector, _incomingSessionMessages, inetSocketAddress);
		_sessions.put(iSession.SessionId(), iSession);
	}
	//todo, cleanup on disconnect, graceful or abrupt
}
