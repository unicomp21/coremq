package coremq;

public interface ILastValueCache {
	ILastValue Get(String topic);
	void Update(String topic, IMessage iMessage);
}
