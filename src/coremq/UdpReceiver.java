package coremq;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.DatagramChannel;

public class UdpReceiver implements IReceiver {
	@SuppressWarnings("unused")
	private UdpReceiver() { }
	private DatagramChannel _socketChannel;
	private IMessageEvent _iMessageEvent;
	private ByteBuffer _incomingBuffer = ByteBuffer.allocate(Constants.MaxMessageSize);
	public UdpReceiver(DatagramChannel socketChannel, IMessageEvent iMessageEvent) {
		_socketChannel = socketChannel;
		_iMessageEvent = iMessageEvent;
	}
	@Override
	public boolean Flush() {
		try {
			InetSocketAddress inetSocketAddress = (InetSocketAddress)_socketChannel.receive(_incomingBuffer);
			if(null != inetSocketAddress) {
				IMessage iMessage = new Message();
				IReadWrite iReadWrite = (IReadWrite)iMessage;
				_incomingBuffer.flip();
				CharBuffer charBuffer = _incomingBuffer.asCharBuffer();
				int charBufferPositionReset = 0;
				while(iReadWrite.Read(charBuffer)) {
					charBufferPositionReset = charBuffer.position();
					_iMessageEvent.Received((IMessage)iReadWrite, inetSocketAddress);
				}
				_incomingBuffer.position(charBufferPositionReset * Constants.SizeOfChar);
				_incomingBuffer.compact();
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
