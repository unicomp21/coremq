package coremq;

import java.nio.channels.spi.AbstractSelector;

public interface IMultiplexor {
	AbstractSelector Selector();
	void Flush(int timeout);
}
