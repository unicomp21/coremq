package coremq;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;

public class TcpReceiver implements IReceiver {
	@SuppressWarnings("unused")
	private TcpReceiver() { }
	private SocketChannel _socketChannel;
	private IMessageEvent _iMessageEvent;
	private ByteBuffer _incomingBuffer = ByteBuffer.allocate(Constants.MaxMessageSize);
	public TcpReceiver(SocketChannel socketChannel, IMessageEvent iMessageEvent) {
		_socketChannel = socketChannel;
		_iMessageEvent = iMessageEvent;
	}
	@Override
	public boolean Flush() {
		try {
			int count = _socketChannel.read(_incomingBuffer);
			if(count > 0) {
				IReadWrite iReadWrite = new Message();
				_incomingBuffer.flip();
				CharBuffer charBuffer = _incomingBuffer.asCharBuffer();
				int charBufferPositionReset = 0;
				while(iReadWrite.Read(charBuffer)) {
					charBufferPositionReset = charBuffer.position();
					_iMessageEvent.Received((IMessage)iReadWrite, _socketChannel.getLocalAddress());
				}
				_incomingBuffer.position(charBufferPositionReset * Constants.SizeOfChar);
				_incomingBuffer.compact();
			}
		} catch (IOException e) {
			//e.printStackTrace();
			return false;
		}
		return true;
	}
}
