package coremq;

import java.io.PrintStream;

public interface ITextRender {
	void Dump(PrintStream outputStream, int indent);
}
