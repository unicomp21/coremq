package coremq;

public class Constants {
	public static final int BitsPerByte = 8;
	
	public static final int MaxString = 'z' - 'a';
	public static final int SizeOfChar = 2;
	public static final int MaxMessageSize = 8192;
	public static final int UdpPort = 9000;
	
	public static final String TestAddress = "127.0.0.1";
	public static final int TestTcpPort = 9000;
	public static final int TestServerUdpPort = 10000;
	public static final int TestClientUdpPort = TestServerUdpPort + 1;
}
