package coremq;

import java.io.IOException;
import java.nio.channels.spi.AbstractSelector;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;

public class Multiplexor implements IMultiplexor {
	private Multiplexor() {
	}

	public static IMultiplexor open() {
		try {
			Multiplexor multiplexor = new Multiplexor();
			multiplexor._selector = (AbstractSelector)Selector.open();
			return multiplexor;
		} catch (IOException e) {
			return null;
		}
	}
	private AbstractSelector _selector;
	
	public void Flush(int timeout) {
		try {
			int count = _selector.select(timeout);
			if(count > 0) {
				for(SelectionKey selectionKey : _selector.selectedKeys()) {
					ISocketNotify iNotify = (ISocketNotify)selectionKey.attachment();
					if(null != iNotify) {
						if(selectionKey.isReadable()) {
							iNotify.ReadSignaled();
						}
						if(selectionKey.isWritable()) {
							iNotify.WriteSignaled();
						}
						if(selectionKey.isAcceptable()) {
							iNotify.AcceptSignaled();
						}
						if(selectionKey.isConnectable()) {
							iNotify.ConnectSignaled();
						}
					} else {
						throw new Exception("no attachment");
					}
				}
				_selector.selectedKeys().clear();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public AbstractSelector Selector() {
		return _selector;
	}
}
