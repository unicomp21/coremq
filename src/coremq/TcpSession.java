package coremq;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardSocketOptions;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Queue;
import java.util.UUID;

public class TcpSession implements ISocketNotify, ISession, IMessageEvent {
	private TcpSession() { }
	private SocketChannel _socketChannel;
	private Selector _selector;
	private String _tag;
	private Queue<IMessage> _incomingSessionMessages;
	private TcpSession(Selector selector, SocketChannel socketChannel, 
			Queue<IMessage> incomingSessionMessages, String tag) throws IOException {
		
		_selector = selector;
		_socketChannel = socketChannel;
		_socketChannel.setOption(StandardSocketOptions.TCP_NODELAY, true);
		_socketChannel.configureBlocking(false);
		_socketChannel.register(_selector, SelectionKey.OP_CONNECT | SelectionKey.OP_READ | SelectionKey.OP_WRITE, this);

		_sender = new TcpSender(socketChannel);
		_receiver = new TcpReceiver(socketChannel, this);
		_incomingSessionMessages = incomingSessionMessages;
		_tag = tag;
	}
	public static ISession ServerSession(
			Selector selector, 
			SocketChannel socketChannel, 
			Queue<IMessage> incomingSessionMessages
			) throws IOException {
		TcpSession iSession = new TcpSession(selector, socketChannel, incomingSessionMessages, "server");
		iSession.PostAcceptMessage();
		return iSession;
	}
	public static ISession ClientSession(
			Selector selector, 
			Queue<IMessage> incomingSessionMessages, 
			InetSocketAddress inetSocketAddress
			) throws IOException {
		SocketChannel socketChannel = SocketChannel.open();
		TcpSession iSession = new TcpSession(selector, socketChannel, incomingSessionMessages, "client");
		iSession._socketChannel.connect(inetSocketAddress);
		return iSession;		
	}
	private boolean _connected = true;
	@Override
	public void AcceptSignaled() { /*NOOP*/ }
	@Override
	public void ConnectSignaled() {
		try {
			_connected = _socketChannel.finishConnect();
			PostConnectMessage();
		} catch (IOException e) {
			_connected = false;
			e.printStackTrace();
		}
	}
	private void PostAcceptMessage() {
		IMessage iMessage = new Message();
		iMessage.PutValue(Tags.Command, Tags.Commands.Accepted);
		iMessage.PutValue(Tags.SessionId, SessionId().toString());
		_incomingSessionMessages.add(iMessage);
	}
	private void PostConnectMessage() {
		IMessage iMessage = new Message();
		iMessage.PutValue(Tags.Command, Tags.Commands.Connected);
		iMessage.PutValue(Tags.SessionId, SessionId().toString());
		_incomingSessionMessages.add(iMessage);
	}
	private void PostSessionEraseMessage() {
		IMessage iMessage = new Message();
		iMessage.PutValue(Tags.Command, Tags.Commands.SessionErase);
		iMessage.PutValue(Tags.SessionId, SessionId().toString());
		_incomingSessionMessages.add(iMessage);		
	}
	private IReceiver _receiver;
	@Override
	public void ReadSignaled() {
		if(_connected) {
			_connected = _receiver.Flush();
			if(!_connected)
				PostSessionEraseMessage();
		}
	}
	private ISender _sender;
	@Override
	public void WriteSignaled() {
		if(_connected)
			try {
				_sender.Flush();
			} catch (Exception e) {
				//e.printStackTrace();
				PostSessionEraseMessage();
				_connected = false;
			}
	}
	private String _sessionId = UUID.randomUUID().toString();
	@Override
	public String SessionId() {
		return _sessionId;
	}
	@Override
	public boolean SendMessage(IMessage iMessage) {
		if(_connected) {
			try {
				_sender.SendMessage(iMessage);
			} catch (Exception e) {
				//e.printStackTrace();
				_connected = false;
			}
		}
		return _connected;
	}
	@Override
	public void Received(IMessage iMessage, SocketAddress inetSocketAddress) {
		iMessage.PutValue(Tags.SessionId, SessionId().toString());
		iMessage.PutValue(Tags.Tag, _tag);
		_incomingSessionMessages.add(iMessage);
	}
	@Override
	public void Close() {
		try {
			_socketChannel.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			_socketChannel = null;
		}
	}
}
