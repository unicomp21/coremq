package coremq;

public interface IWebsocketFrameHeader {
	void setFIN(boolean val);
	boolean getFIN();
	void setRSV1(boolean val);
	boolean getRSV1();
	void setRSV2(boolean val);
	boolean getRSV2();
	void setRSV3(boolean val);
	boolean getRSV3();
	void setOpcode(int opcode);
	int getOpcode();
	void setShortPayloadLen(byte val);
	byte getShortPayloadLen();
}
