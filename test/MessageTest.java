import static org.junit.Assert.*;

import java.nio.CharBuffer;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import coremq.IMessage;
import coremq.IReadWrite;
import coremq.ITextRender;
import coremq.Message;

public class MessageTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test1() {
		IMessage iMessageSrc = new Message();
		iMessageSrc.PutValue("tag1", "value1");
		assertEquals("value1", iMessageSrc.GetValue("tag1"));

		CharBuffer charBuffer = CharBuffer.allocate(8192);
		try {
			((IReadWrite) iMessageSrc).Write(charBuffer);
		} catch (Exception e) {
			fail("fail on serialize");
		}

		charBuffer.flip();
		IMessage iMessageDst = new Message();
		((IReadWrite) iMessageDst).Read(charBuffer);
		assertEquals("value1", iMessageDst.GetValue("tag1"));
	}

	@Test
	public void test2() {
		IMessage iMessageSrc = new Message();
		iMessageSrc.PutValue("tag1", "value1");
		assertEquals("value1", iMessageSrc.GetValue("tag1"));

		IMessage iMessageSrcChild = new Message();
		iMessageSrcChild.PutValue("tag2", "value2");
		iMessageSrc.PutMessage("child1", iMessageSrcChild);
		iMessageSrcChild = iMessageSrc.GetMessage("child1");
		assertEquals("value2", iMessageSrcChild.GetValue("tag2"));

		CharBuffer byteBuffer = CharBuffer.allocate(8192);
		try {
			((IReadWrite) iMessageSrc).Write(byteBuffer);
		} catch (Exception e) {
			fail("fail on serialize");
		}

		byteBuffer.flip();
		IMessage iMessageDst = new Message();
		((IReadWrite) iMessageDst).Read(byteBuffer);
		assertEquals("value1", iMessageDst.GetValue("tag1"));

		IMessage iMessageDstChild = iMessageDst.GetMessage("child1");
		assertEquals("value2", iMessageDstChild.GetValue("tag2"));
		
		((ITextRender)iMessageDst).Dump(System.out, 0);
	}
}
