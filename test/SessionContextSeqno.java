import java.util.Hashtable;


class SessionContextSeqno {
	public SessionContextSeqno() { }
	private Hashtable<String /*session id*/, Integer> sessionContext = new Hashtable<String, Integer>();
	public int GetValue(String sessionId) {
		return sessionContext.get(sessionId);
	}
	public void PutValue(String sessionId, int seqno) {
		sessionContext.put(sessionId, seqno);
	}
}
