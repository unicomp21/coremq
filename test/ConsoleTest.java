import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Date;

import coremq.IMultiplexor;
import coremq.ISessionsMessaging;
import coremq.Multiplexor;
import coremq.TcpClientSessions;
import coremq.TcpServerSessions;

public class ConsoleTest {
	private static void Usage() {
		System.out.println("Usage:");
		System.out.println("java ConsoleTest /server_tcp_echo [listen_ip_address] [listen_ip_port]");
		System.out.println("java ConsoleTest /client_tcp_echo [server_ip_address] [server_ip_port] [echo_count]");		
	}
	private static void ServerTcpEcho(InetSocketAddress inetSocketAddressListen) {
		System.out.println("ServerTcpEcho: " + inetSocketAddressListen.toString());
		
		IMultiplexor iMultiplexor = Multiplexor.open();

		// multiplex tcp sockets concurrently all on a single thread, non-blocking
		try {
			// tcp server ping/pong connections
			ISessionsMessaging serverSessionsMessagingTcp = TcpServerSessions.open(
					inetSocketAddressListen, iMultiplexor.Selector());
			ServerEcho serverEchoTcp = new ServerEcho(serverSessionsMessagingTcp);

			// ping/pong round trips
			for(;;) {
				iMultiplexor.Flush(100);
				serverEchoTcp.Flush();
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
			fail("failed");
		}
	}
	private static void ClientsTcpEcho(InetSocketAddress inetSocketAddressConnect, int connectionCount) {
		System.out.println("ClientsTcpEcho: " + inetSocketAddressConnect.toString());

		IMultiplexor iMultiplexor = Multiplexor.open();

		// multiplex tcp sockets concurrently all on a single thread, non-blocking
		try {
			// tcp clients ping/pong connections
			ISessionsMessaging clientSessionsMessagingTcp = TcpClientSessions.open(iMultiplexor.Selector());
			ClientsEcho clientsEchoTcp = new ClientsEcho(clientSessionsMessagingTcp, 1000);
			for(int i = 0; i < connectionCount; i++) {
				clientSessionsMessagingTcp.Connect(inetSocketAddressConnect);
				Thread.sleep(1); // throttle connection rate
				iMultiplexor.Flush(0);
			}
			
			// time ping/pong round trips
			Date start = new Date();
			for(;;) {
				iMultiplexor.Flush(100);
				if(!clientsEchoTcp.Flush(false))
					break;
			}
			Date end = new Date();
			
			// dump stats
			long elapsedMilliseconds = end.getTime() - start.getTime();
			final int milliSecondsPerSecond = 1000;
			double rate = (clientsEchoTcp.MessageCount()) * milliSecondsPerSecond;
			rate /= elapsedMilliseconds;
			System.out.println(Double.toString(rate) + " messages/s (tcp)");
		} catch (Exception e) {
			e.printStackTrace(System.out);
			fail("failed");
		}
	}
	public static void main(String[] args) throws IOException {
		try {
			switch(args[0]) {
			case "/server_tcp_echo":
				ServerTcpEcho(new InetSocketAddress(args[1], Integer.parseInt(args[2])));
				break;
			case "/client_tcp_echo":
				ClientsTcpEcho(new InetSocketAddress(args[1], Integer.parseInt(args[2])), Integer.parseInt(args[3]));
				break;
			default:
				Usage();
				break;
			}
		} catch(Exception e) {
			Usage();
		}
	}
}
