import static org.junit.Assert.assertEquals;
import coremq.IMessage;
import coremq.ISessionsMessaging;
import coremq.Tags;
import coremq.Tags.Commands;


class ClientsEcho {
	private SessionContextSeqno _sessionContextSeqno = new SessionContextSeqno();
	private ISessionsMessaging _iSessionsMessaging;
	@SuppressWarnings("unused")
	private ClientsEcho() { }
	public ClientsEcho(ISessionsMessaging iSessionsMessaging, int roundTrips) {
		_iSessionsMessaging = iSessionsMessaging;
		_roundTrips = roundTrips;
	}
	public int _roundTrips;
	public boolean Flush(boolean withAsserts) {
		IMessage iMessage = _iSessionsMessaging.GetMessage();
		while(null != iMessage) {
			String command = iMessage.GetValue(Tags.Command);
			String sessionId = iMessage.GetValue(Tags.SessionId);
			//((ITextRender)iMessage).Dump(System.out, 0);
			if(Tags.Commands.Connected.equals(command)) {
				// first ping
				final int seqno = 0;
				iMessage.PutValue(Tags.Command, Commands.Echo);
				iMessage.PutValue(Tags.SeqNo, Integer.toString(seqno));
				_sessionContextSeqno.PutValue(sessionId, seqno);
				_iSessionsMessaging.SendMessage(iMessage);
			} else if(Tags.Commands.Echo.equals(command)) {
				// subsequent pings
				int seqno = _sessionContextSeqno.GetValue(sessionId);
				if(seqno > _roundTrips)
					return false;
				if(withAsserts)
					assertEquals(seqno + 1, Integer.parseInt(iMessage.GetValue(Tags.SeqNo)));
				seqno += 2;
				iMessage.PutValue(Tags.SeqNo, Integer.toString(seqno));
				_iSessionsMessaging.SendMessage(iMessage);
				_messageCount++;
				_sessionContextSeqno.PutValue(sessionId, seqno);
			}
			iMessage = _iSessionsMessaging.GetMessage();
			_messageCount++;
		}
		return true;
	}
	private int _messageCount = 0;
	public int MessageCount() {
		return _messageCount;
	}
}

