import static org.junit.Assert.*;

import java.net.InetSocketAddress;
import java.util.Date;

import org.junit.Test;

import coremq.TcpClientSessions;
import coremq.Constants;
import coremq.IMultiplexor;
import coremq.ISessionsMessaging;
import coremq.Multiplexor;
import coremq.TcpServerSessions;
import coremq.UdpSessions;


public class ClientServerStressTest {

	@Test
	public void testTcp() {
		IMultiplexor iMultiplexor = Multiplexor.open();

		// multiplex tcp sockets concurrently all on a single thread, non-blocking
		try {
			// tcp server ping/pong connections
			ISessionsMessaging serverSessionsMessagingTcp = TcpServerSessions.open(
					new InetSocketAddress(Constants.TestAddress, Constants.TestTcpPort), iMultiplexor.Selector());
			ServerEcho serverEchoTcp = new ServerEcho(serverSessionsMessagingTcp);

			// tcp clients ping/pong connections
			ISessionsMessaging clientSessionsMessagingTcp = TcpClientSessions.open(iMultiplexor.Selector());
			ClientsEcho clientsEchoTcp = new ClientsEcho(clientSessionsMessagingTcp, 1000);
			final int clientConnectionCount = 100;
			for(int i = 0; i < clientConnectionCount; i++) {
				clientSessionsMessagingTcp.Connect(new InetSocketAddress(Constants.TestAddress, Constants.TestTcpPort));
				serverEchoTcp.Flush(); // pump async socket accepts while connects come in
				iMultiplexor.Flush(0);
			}
			
			// time ping/pong round trips
			Date start = new Date();
			for(;;) {
				iMultiplexor.Flush(100);
				serverEchoTcp.Flush();
				if(!clientsEchoTcp.Flush(true))
					break;
			}
			Date end = new Date();
			
			// dump stats
			long elapsedMilliseconds = end.getTime() - start.getTime();
			final int milliSecondsPerSecond = 1000;
			double rate = (serverEchoTcp.MessageCount() + clientsEchoTcp.MessageCount()) * milliSecondsPerSecond;
			rate /= elapsedMilliseconds;
			System.out.println(Double.toString(rate) + " messages/s (tcp)");
		} catch (Exception e) {
			e.printStackTrace(System.out);
			fail("failed");
		}
	}

	@Test
	public void testUdp() {
		IMultiplexor iMultiplexor = Multiplexor.open();

		// multiplex multiple tcp and udp sockets concurrently all on a single thread, non-blocking
		try {
			// udp server ping/pong todo
			ISessionsMessaging serverSessionsMessagingUdp = UdpSessions.open(
					iMultiplexor.Selector(),
					new InetSocketAddress(Constants.TestAddress, Constants.TestServerUdpPort));
			ServerEcho serverEchoUdp = new ServerEcho(serverSessionsMessagingUdp);
			
			// udp client ping/pong todo
			ISessionsMessaging clientSessionsMessagingUdp = UdpSessions.open(
					iMultiplexor.Selector(),
					new InetSocketAddress(Constants.TestAddress, Constants.TestClientUdpPort));
			ClientsEcho clientsEchoUdp = new ClientsEcho(clientSessionsMessagingUdp, 100000);
			clientSessionsMessagingUdp.Connect(new InetSocketAddress(Constants.TestAddress, Constants.TestServerUdpPort));

			// time ping/pong round trips
			Date start = new Date();
			for(;;) {
				iMultiplexor.Flush(100);
				serverEchoUdp.Flush();
				if(!clientsEchoUdp.Flush(false))
					break;
			}
			Date end = new Date();
			
			// dump stats
			long elapsedMilliseconds = end.getTime() - start.getTime();
			final int milliSecondsPerSecond = 1000;
			double rate = (serverEchoUdp.MessageCount() + clientsEchoUdp.MessageCount()) * milliSecondsPerSecond;
			rate /= elapsedMilliseconds;
			System.out.println(Double.toString(rate) + " messages/s (udp)");
		} catch (Exception e) {
			e.printStackTrace(System.out);
			fail("failed");
		}
	}

}
