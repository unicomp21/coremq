import coremq.IMessage;
import coremq.ISessionsMessaging;
import coremq.Tags;


class ServerEcho {
	private ISessionsMessaging _iSessionsMessaging;
	@SuppressWarnings("unused")
	private ServerEcho() { }
	public ServerEcho(ISessionsMessaging iSessionsMessaging) { _iSessionsMessaging = iSessionsMessaging; }
	public void Flush() {
		IMessage iMessage = _iSessionsMessaging.GetMessage();
		while(null != iMessage) {
			String command = iMessage.GetValue(Tags.Command);
			//((ITextRender)iMessage).Dump(System.out, 0);
			if(Tags.Commands.Accepted.equals(command)) {
				// do nothing
			} else if(Tags.Commands.Echo.equals(command)){
				// send pong
				int seqno = Integer.parseInt(iMessage.GetValue(Tags.SeqNo)) + 1;
				iMessage.PutValue(Tags.SeqNo, Integer.toString(seqno));
				_iSessionsMessaging.SendMessage(iMessage);
				_messageCount++;
			}
			iMessage = _iSessionsMessaging.GetMessage();
			_messageCount++;
		}
	}
	int _messageCount = 0;
	public int MessageCount() {
		return _messageCount;
	}
}

