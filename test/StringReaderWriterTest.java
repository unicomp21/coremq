import static org.junit.Assert.*;

import java.nio.CharBuffer;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import coremq.Constants;
import coremq.IPrefixedStringReader;
import coremq.IPrefixedStringWriter;
import coremq.PrefixedStringReader;
import coremq.PrefixedStringWriter;


public class StringReaderWriterTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test1() {
		CharBuffer buffer = CharBuffer.allocate(Constants.MaxMessageSize / 2);
		IPrefixedStringWriter iPrefixedStringWriter = new PrefixedStringWriter(buffer);
		try {
			iPrefixedStringWriter.WriteShortString("short_string");
		} catch (Exception e) {
			fail("write short string");
		}
		buffer.flip();
		IPrefixedStringReader iPrefixedStringReader = new PrefixedStringReader(buffer);
		assertEquals("short_string", iPrefixedStringReader.ReadShortString());
	}

	@Test
	public void test2() {
		CharBuffer buffer = CharBuffer.allocate(Constants.MaxMessageSize / 2);
		IPrefixedStringWriter iPrefixedStringWriter = new PrefixedStringWriter(buffer);
		try {
			iPrefixedStringWriter.WriteLargeString("short_string");
		} catch (Exception e) {
			fail("write short string");
		}
		buffer.flip();
		IPrefixedStringReader iPrefixedStringReader = new PrefixedStringReader(buffer);
		assertEquals("short_string", iPrefixedStringReader.ReadLargeString());
	}

}
