# CoreMQ
How to multiplex thousands of tcp socket using Java NIO on a single thread

#Nutshell Explanation
If we think of all application state as a tree, much like a filesystem, where each individual object instance has a path, we can build large distributed systems using two calls.
publish(object_path, object_state);
and
subscribe(object_path, object_changed_callback);
where object_state_callback looks like:
object_changed_callback(object_path, object_state);

#Messaging Basis, Length Prefixed Strings
CoreMQ uses Length Prefixed Strings, LPS, pronounced "lips".
There are two kinds of length prefixed strings, short and large. Short strings are encoded as follows. The length prefix character is a-z, mapping to the value 0-25. Encoding the string "dog" into a short LPS, LPS_SHORT, becomes "edog" where the first e=3 and indicates three characters follow. The same scheme can be used to encode integers. An integer like "1234" is encoded as "f1234" where f=4 and indicates the integer contains four characters. What about strings exceeding twenty five characters? We call strings larger than twenty five characters large strings, LPS_LARGE, and simply prefix the large string with a short string containing an integer, the dynamic length integer indicates the length of the large string.
For those familiar with the FIX protocol, if I could rewrite it, LPS would be my answer.

#LPS Message Format
CoreMQ messages are based on LPS, length prefixed strings, to compose an LPS_MESSAGE. Message layout is as follows: LPS_SHORT (tag_value_count) | LPS_SHORT(tag1) | LPS_LARGE(value1) | LPS_SHORT(tag2) | LPS_LARGE(value2) | ...
To support hierarchy we create a tree of LPS_MESSAGE, to compose an LPS_TAG_MESSAGE tree. For now, see the code, Message.java, for the details.
